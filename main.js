// JavaScript logic for the Chrome dinosaur game
document.addEventListener("keydown", function (event) {
  if (event.code === "Space") {
    jump();
  }
});

function jump() {
  const dinosaur = document.querySelector(".dinosaur");
  dinosaur.style.animation = "jump 1s";
  setTimeout(function () {
    dinosaur.style.animation = "";
  }, 1000);
}
